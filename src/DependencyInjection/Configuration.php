<?php

namespace AzureSpring\Bundle\BincampBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        if (method_exists(TreeBuilder::class, 'root')) {
            $treeBuilder = new TreeBuilder();
            $rootNode = $treeBuilder->root('azurespring_bincamp');
        } else {
            $treeBuilder = new TreeBuilder('azurespring_bincamp');
            $rootNode = $treeBuilder->getRootNode();
        }

        $rootNode
            ->children()
                ->scalarNode('public_dir')
                    ->defaultValue('public')
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
