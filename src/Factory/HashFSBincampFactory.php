<?php

namespace AzureSpring\Bundle\BincampBundle\Factory;

use AzureSpring\Bincamp\HashFSBincamp;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class HashFSBincampFactory
{
    private $router;

    private $fs;

    private $root;


    public function __construct( UrlGeneratorInterface $router, Filesystem $fs, $root )
    {
        $this->router = $router;
        $this->fs     = $fs;
        $this->root   = $root;
    }

    public function createHashFSBincamp()
    {
        $path = $this->router->generate( 'azurespring_bincamp_blobs_create' );

        return new HashFSBincamp( $this->fs, $this->root . $path, $path );
    }
}
